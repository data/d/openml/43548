# OpenML dataset: All-Time-Premier-League-Player-Statistics

https://www.openml.org/d/43548

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
I am a really huge football fan and the Premier League is one of my favourite football (or soccer, whatever you like to call it) leagues. So, as my very first dataset, I thought this would be a great opportunity for me to make a dataset of player statistics of all seasons from the Premier League.
The Premier League, often referred to as the English Premier League or the EPL outside England, is the top level of the English football league system. Contested by 20 clubs, it operates on a system of promotion and relegation with the English Football League (EFL). Contested by 20 clubs, it operates on a system of promotion and relegation with the English Football League. 
Home to some of the most famous clubs, players, managers and stadiums in world football, the Premier League is the most-watched league on the planet with one billion homes watching the action in 188 countries.The league takes place between August and May and involves the teams playing each other home and away across the season, a total of 380 matches.
Three points are awarded for a win, one point for a draw and none for a defeat, with the team with the most points at the end of the season winning the Premier League title. The teams that finish in the bottom three of the league table at the end of the campaign are relegated to the Championship, the second tier of English football. Those teams are replaced by three clubs promoted from the Championship; the sides that finish in first and second place and the third via the end-of-season playoffs. 
Details about the dataset

Some players of certain position may not have certain statistics - For example, A goalkeeper may not have a statistic for "Shot Accuracy"
The format for the filename is - dataset - yyyy-mm-dd Date
(The date is date when the file was last updated on)

Content
The data was acquired from:
https://www.premierleague.com/ 
I made a BeautifulSoup4 Web Scrapper in Python3 which automatically outputs a csv file of all the player statistics. The runtime of the file is about 20 minutes but it varies with the bandwidth of the Internet connection. I made this program so that this dataset could be updated weekly. The reason for weekly update is that the statistics change after each match played by the player so I felt that for the most up-to-date results, such a program is needed. Planning this project took 2 days. Making the program in Python3 took 7 days and the testing and bug fixing took another 5 days. The project was completed in the span of 2 weeks.
Acknowledgements
Source credits : https://www.premierleague.com/
Image credits : https://rb.gy/wuiwth
Inspiration
How do variables like age, nationality and club affect the player performance? 
Known issues in the dataset

Goals per match displays an abnormally high value for a few players as the HTML displays incorrect value during first few milliseconds of loading the page. I am trying to fix it analytically rather than scrapping directly from the website.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43548) of an [OpenML dataset](https://www.openml.org/d/43548). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43548/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43548/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43548/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

